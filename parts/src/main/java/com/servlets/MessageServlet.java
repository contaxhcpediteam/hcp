package com.servlets;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.as2.modify.CertificateManagement;
import com.helger.as2lib.crypto.ECryptoAlgorithmSign;
import com.helger.as2lib.util.cert.CertificateException;
import com.helger.commons.io.file.FileHelper;
import com.helger.peppol.as2client.AS2ClientBuilderException;

//import com.as2.modify.AS2Configurations;
//import com.as2.modify.CertificateManagement;
//import com.as2.modify.SendAS2Message;
//import com.helger.as2lib.crypto.ECryptoAlgorithmSign;
//import com.helger.as2lib.message.AS2Message;
//import com.helger.as2lib.util.cert.CertificateException;
//import com.helger.as2servlet.AS2ReceiveServlet;
//import com.helger.commons.io.file.FileHelper;
//import com.helger.peppol.as2client.AS2ClientBuilderException;
//import com.helger.web.fileupload.IFileItem;
//import com.helger.web.fileupload.exception.FileUploadException;
//import com.helger.web.fileupload.io.DiskFileItemFactory;
//import com.helger.web.fileupload.servlet.ServletFileUpload;

/**
 * Servlet implementation class MessageServlet
 */
@WebServlet({ "/MessageServlet", "/createmsg", "/deletemsg", "/sendmsg" })
@MultipartConfig
public class MessageServlet extends HttpServlet {
	
	
	private Logger s_aLogger = LoggerFactory.getLogger (MessageServlet.class);
    
	private static final long serialVersionUID = 656675084400738444L;

	/**
     * @see HttpServlet#HttpServlet()
     */
    public MessageServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

//			Initialize Global configurtation    
		
			String realpath =  getServletContext().getRealPath(File.separator);
			GlobalConfiguration config = new GlobalConfiguration(realpath);
		
//			Set Varibles used for sending a message
		
		    boolean isMultipart = ServletFileUpload.isMultipartContent(request);
		 	String m_sAS2Subject = "";
			String m_sSenderAS2ID = "";
			String m_sSenderAS2Email = "";
			String m_sSenderAS2KeyAlias = "";
			String m_sReceiverAS2ID = "";
			String m_sReceiverAS2KeyAlias = "";
			String m_sReceiverAS2Url = "";
			File m_aPayload = null;
			ECryptoAlgorithmSign m_eSigningAlgoc = null;
//			X509Certificate m_asReceiverCert;
			
//			Prepare temp folder to hold payload of message for now it has to a .pem file becuase of manual certificate entry
			
			
	    if (isMultipart) {
	        DiskFileItemFactory factory = new DiskFileItemFactory();
	        ServletFileUpload upload = new ServletFileUpload(factory);
	        
//	        s_aLogger.error(config.getTmpFolder());
	        Path tmpFolder = Paths.get(config.getTmpFolder() + "/");
			File repository = new File(tmpFolder.toString());
			factory.setRepository(repository);
			
//			Process the fields that are passed trough the form application and map values to an Hashtable
			
	    try {
	    	
	    	Hashtable<String, String> ht = new Hashtable<String, String>();
	        List<FileItem> items = upload.parseRequest(request);
	        Iterator<FileItem> iterator = items.iterator();
	        while (iterator.hasNext()) {
	            FileItem item = (FileItem) iterator.next();

	            if (!item.isFormField()) {
	                

	                String fileName = item.getName();
	                if (fileName != null) {
	                	fileName = FilenameUtils.getName(fileName);
	                }
	                
	                File uploadedFile = new File(tmpFolder + "/" + fileName);
	                item.write(uploadedFile);
	                m_aPayload = uploadedFile;
	                s_aLogger.error(" The name of the file = "+uploadedFile.getName() + "And is stored in: " + uploadedFile.getAbsolutePath());
	                
	            } else
	            	
	            {
	            	 ht.put(item.getFieldName(), item.getString());
	            	 
	            }
	        }
	        
//	        Assign hashtable values to varibles
	        
	         m_sAS2Subject = ht.get("subject");
			 m_sSenderAS2ID = ht.get("sendID");
			 m_sSenderAS2Email = ht.get("sendMail");
			 m_sSenderAS2KeyAlias = ht.get("sendKey");
			 m_sReceiverAS2ID = ht.get("recieveID");
			 m_sReceiverAS2KeyAlias = ht.get("receiveAlias");
			 m_sReceiverAS2Url = ht.get("recieveURL");
			 m_eSigningAlgoc=ECryptoAlgorithmSign.getFromIDOrNull( ht.get("signALG"));
	         s_aLogger.error(" The filename =  " + m_aPayload.getName());
	        
	    } catch (FileUploadException e) {
	        e.printStackTrace();
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
    
	}
	    
//	    	Call class that will handle the message input and assign the variables to the class
//		
	    	SendAS2Message sender = new SendAS2Message(config);
			sender.setPayload(m_aPayload);
			sender.setAS2Subject(m_sAS2Subject);
			sender.setSenderAS2ID(m_sSenderAS2ID);
			sender.setSenderAS2Email(m_sSenderAS2Email);
			sender.setSenderAS2KeyAlias(m_sSenderAS2KeyAlias);
			sender.setReceiverAS2ID(m_sReceiverAS2ID);
			sender.setReceiverAS2KeyAlias(m_sReceiverAS2KeyAlias);
			sender.setReceiverAS2Url(m_sReceiverAS2Url);
			sender.setAS2SigningAlgorithm(m_eSigningAlgoc);
			try {
				sender.setReceiverCertificate(CertificateManagement.convertToX509Certificate(FileHelper.getInputStream(m_aPayload)));
			} catch (CertificateException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		
		
		
		
			try {
				sender.sendSynchronous();
			} catch (AS2ClientBuilderException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
//		
	}

}
