package com.servlets;

import java.io.IOException;
import java.io.Writer;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Servlet implementation class PassServletContextServlet
 */
@WebServlet(
		description = "Test to pass context to other classes", 
		urlPatterns = { 
				"/PassServletContextServlet", 
				"/pass"
		})
public class PassServletContextServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Logger s_aLogger = LoggerFactory.getLogger (this.getClass());
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PassServletContextServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		
		String realpath =  getServletContext().getRealPath("");
		GlobalConfiguration config = new GlobalConfiguration(realpath);
		
		
		
		
		
		
//		config.setServletContextPath(request.getContextPath()).toString();
		config.getAs2ConfigFolder();
		s_aLogger.debug(config.getServletContextPath());
		String t = config.getRecourceConfigfFolder() + config.getKeystoreFileName();
		Writer writer = response.getWriter();
		writer.write(config.getAs2ConfigFolder() + "</br >" +
					config.getClassFolder() + "</br >" +
					config.getCssFolder() + "</br >" +
					config.getJsFolder() + "</br >" +
					config.getTmpFolder() + "</br >" +
					config.getServletRealPath() + "</br >" +
					config.getKeystorePath() + "</br >"+
					config.getKeystorePassword() +"</br >"+
					config.getResource(config.getKeystoreFileName()) +"</br >"+
				 t
				);
		
		
	}

}
