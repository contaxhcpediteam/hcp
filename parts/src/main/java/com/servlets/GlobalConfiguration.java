package com.servlets;

import java.io.File;
//import java.io.File;
import java.io.InputStream;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.helger.commons.microdom.IMicroDocument;
import com.helger.commons.microdom.IMicroElement;
import com.helger.commons.microdom.serialize.MicroReader;
import com.helger.commons.io.file.FileHelper;

public class GlobalConfiguration {
	
	private String ServletContextPath;
	public String ServletContextRealPath;

	private Logger s_aLogger = LoggerFactory.getLogger (this.getClass());
	
	//Configure paths here that are often used and call them trough get methods
	
	private String AS2resourceFolder = "/WEB-INF/classes/configurations";
	private String classFolder 	= "/WEB-INF/classes";
	private String tmpFolder 	= "/resources/tmp";
	private String cssFolder 	= "/resources/css";
	private String jsFolder  	= "/resources/js";
	
	//Configure sub folders here used for the getResource() method for single point of change
	
	private String recourceConfigfFolder = "/configuration";
	
	
	// Configure Files Used in the application resources folder
	private String As2config = "/AS2config.xml";
	private String keystoreFileName  = "/server-certs.p12";
	private String keystorePass  = "";	
	
	/**
	 * Requires String realpath =  getServletContext().getRealPath(""); at servlet to build path strings from GlobalConfiguration 
	 */
	public GlobalConfiguration(String context)
	{
		this.setServletRealPath(context);
	}
	
	
	/**
	 * 27-05-16 Does not work yet! to do
	 * Requires String with leading slash ' / ' and path to resource. if resource is in subfolder then use /<subfoldername>/<filename> 
	 */
	URL getResource(String Filename)
	{
		
		URL resource = this.getClass().getResource(Filename);
		return resource;
	}
	
	String getAs2ConfigFolder()
	{
		String tmp = this.getServletRealPath() + this.AS2resourceFolder;
		return tmp;
	}
	
	String getClassFolder()
	{
		String tmp = this.getServletRealPath() + this.classFolder;
		return tmp;
	}
	
	String getTmpFolder()
	{
		String tmp = this.getServletRealPath() + this.tmpFolder;
		return tmp;
	}

	String getCssFolder()
	{
		String tmp = this.getServletRealPath() + this.cssFolder;
		return tmp;
	}
	
	String getJsFolder()
	{
		String tmp = this.getServletRealPath() + this.jsFolder;
		return tmp;
	}
	
	String getKeystorePath()
	{
		
		String keystorePath = this.getAs2ConfigFolder() + this.As2config;
		
		// check if the given keystore Filename in GlobalConfiguration is Matching AS2Open framework config file
		this.CheckFrameworkKeystoreValues(FileHelper.getInputStream(keystorePath));
		return this.getAs2ConfigFolder() + this.keystoreFileName;
	}
	
	String getKeystoreFileName() {
		return this.keystoreFileName;
	}
	
	File getKeyStore ()
	{

		File keystore = new File(this.getAs2ConfigFolder() + this.keystoreFileName);
		return keystore;
	}
	
	String getKeystorePassword()
	{
		
//		String keystorePassword = this.getAs2ConfigFolder() + this.As2config;
		File keystorePassword = new File(this.getAs2ConfigFolder() + this.As2config);
		
		if (keystorePassword.isFile())
		{
			s_aLogger.error("new file is created for as2config in password method");
		} else {
			s_aLogger.error("new file is could not be created for path " + this.getAs2ConfigFolder() + this.As2config);
		}
		
		// check if the given keystore Password in GlobalConfiguration is Matching AS2Open framework config file
		this.CheckFrameworkKeystoreValues(FileHelper.getInputStream(keystorePassword));
		return this.keystorePass;
		
	}
	
	String setServletContextPath(String path)
	{
		return this.ServletContextPath = path;
	}
	
	String getServletContextPath()
	{
		return this.ServletContextPath;
	}
	
	String setServletRealPath(String realpath) 
	{
		
		return 	this.ServletContextRealPath = realpath;
		
	}
	
	String getServletRealPath() 
	{
		
		return 	this.ServletContextRealPath;
		
	}
	
	
	
	public String getRecourceConfigfFolder() {
		return this.recourceConfigfFolder;
	}



	public void CheckFrameworkKeystoreValues (InputStream aIS) 
	  {
		  
//		  final String EL_CERTIFICATES = "certificates";
		  String FrameworkKeystoreFileName = "filename";
		  String FrameworkPassword = "password";
		  s_aLogger.debug(aIS.toString());
		  
		  
		  final IMicroDocument aDoc = MicroReader.readMicroXML (aIS);
		  final IMicroElement eRoot = aDoc.getDocumentElement ();
		  if (s_aLogger.isDebugEnabled())
		  {
		  s_aLogger.debug("Input document has the following data " + aDoc.toString());
		  s_aLogger.debug("The document contains the followinging root Elements" + eRoot.toString());
		  }
		  
		  for (final IMicroElement eRootChild : eRoot.getAllChildElements ())
		    {
//		      final String sNodeName = eRootChild.getTagName ();
		     
		      for (final String eAttribute : eRootChild.getAllAttributeNames())
		      {
		    	  
		    	  if (eAttribute.equals(FrameworkKeystoreFileName))
		    	  {
		    		  FrameworkKeystoreFileName = eRootChild.getAttributeValue(eAttribute).substring(6);
		    		  
		    		  s_aLogger.debug("Keystore value in config file = "+ FrameworkKeystoreFileName);
		    		  
		    		  
		    	  }
		    	  
		    	  if (eAttribute.equals(FrameworkPassword))
		    	  {
		    		  FrameworkPassword = eRootChild.getAttributeValue(eAttribute);
		    		  s_aLogger.debug("Keystore value in config file = "+ FrameworkPassword);
		    	  }
		      }
		     
		    }
		  
		  if (!FrameworkKeystoreFileName.equals(this.keystoreFileName))
	      {
	    	  this.keystoreFileName = FrameworkKeystoreFileName;
	    	  s_aLogger.debug("Keystore filename in GlobalConfigurations class does not match AS2Config File and has set As2Config Filename as defualth");
	      } else {
	    	  
	    	  s_aLogger.debug("Keystore filename in GlobalConfigurations is matching As2 config file in resources folder");
	      }
	      
	      if (!FrameworkPassword.equals(this.keystorePass))
	      {
	    	  this.keystorePass = FrameworkPassword;
	    	  s_aLogger.debug("Keystore password in GlobalConfigurations does not matching As2 config file in resources folder");
	      } else {
	    	  
	    	  s_aLogger.debug("Keystore password in GlobalConfigurations is matching As2 config file in resources folder");
	      }
		  
	  }

	

}
