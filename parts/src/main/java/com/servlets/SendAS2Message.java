package com.servlets;

import java.io.File;
import java.security.cert.X509Certificate;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.OverridingMethodsMustInvokeSuper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.helger.as2lib.cert.IStorableCertificateFactory;
import com.helger.as2lib.client.AS2Client;
import com.helger.as2lib.client.AS2ClientRequest;
import com.helger.as2lib.client.AS2ClientResponse;
import com.helger.as2lib.client.AS2ClientSettings;
import com.helger.as2lib.crypto.ECryptoAlgorithmSign;
import com.helger.as2lib.disposition.DispositionOptions;
import com.helger.commons.ValueEnforcer;
import com.helger.commons.email.EmailAddressHelper;
import com.helger.commons.factory.FactoryNewInstance;
import com.helger.commons.factory.IFactory;
import com.helger.commons.string.StringHelper;
import com.helger.commons.url.URLHelper;
import com.helger.peppol.as2client.AS2ClientBuilderException;
import com.helger.peppol.as2client.DefaultAS2ClientBuilderMessageHandler;
import com.helger.peppol.as2client.IAS2ClientBuilderMessageHandler;

public class SendAS2Message {
	
	  private GlobalConfiguration config;
	  
	  public static final String DEFAULT_AS2_SUBJECT = "OpenPEPPOL AS2 message";
	  @SuppressWarnings("deprecation")
	public static final ECryptoAlgorithmSign DEFAULT_SIGNING_ALGORITHM = ECryptoAlgorithmSign.DIGEST_SHA1;
	  public static final String DEFAULT_AS2_MESSAGE_ID_FORMAT = "OpenPEPPOL-$date.ddMMyyyyHHmmssZ$-$rand.1234$@$msg.sender.as2_id$_$msg.receiver.as2_id$";

	  private static final Logger s_aLogger = LoggerFactory.getLogger (SendAS2Message.class);

	  private IAS2ClientBuilderMessageHandler m_aMessageHandler = new DefaultAS2ClientBuilderMessageHandler ();
	  private File m_aKeyStoreFile;
	  private File m_aPayload;
	  private String m_sKeyStorePassword;
	  private boolean m_bSaveKeyStoreChangesToFile = IStorableCertificateFactory.DEFAULT_SAVE_CHANGES_TO_FILE;
	  private String m_sAS2Subject = DEFAULT_AS2_SUBJECT;
	  private String m_sSenderAS2ID;
	  private String m_sSenderAS2Email;
	  private String m_sSenderAS2KeyAlias;
	  private String m_sReceiverAS2ID;
	  private String m_sReceiverAS2KeyAlias;
	  private String m_sReceiverAS2Url;
	  private X509Certificate m_aReceiverCert;
	  private ECryptoAlgorithmSign m_eSigningAlgo = DEFAULT_SIGNING_ALGORITHM;
	  private String m_sMessageIDFormat = DEFAULT_AS2_MESSAGE_ID_FORMAT;
	  
	  private IFactory <AS2Client> m_aAS2ClientFactory = FactoryNewInstance.create (AS2Client.class, true);
	
	public SendAS2Message(GlobalConfiguration globalConfiguration)
	{
		this.config = globalConfiguration;
		this.setPKCS12KeyStore(config.getKeyStore(), config.getKeystorePassword());
	}
	
	@Nonnull
	  protected final IAS2ClientBuilderMessageHandler getMessageHandler ()
	  {
	    return m_aMessageHandler;
	  }

	  /**
	   * Set the message handler to be used by the {@link #verifyContent()} method.
	   * By default an instance of {@link DefaultAS2ClientBuilderMessageHandler} is
	   * used so this method should only be called if you have special auditing
	   * requirements.
	   *
	   * @param aMessageHandler
	   *        The message handler to be used. May not be <code>null</code>.
	   * @return this for chaining
	   */
	  @Nonnull
	  public SendAS2Message setMessageHandler (@Nonnull final IAS2ClientBuilderMessageHandler aMessageHandler)
	  {
	    m_aMessageHandler = ValueEnforcer.notNull (aMessageHandler, "MessageHandler");
	    return this;
	  }

	  /**
	   * Set the key store file and password for the AS2 client. The key store must
	   * be an existing file of type PKCS12 containing at least the key alias of the
	   * sender (see {@link #setSenderAS2ID(String)}). The key store file must be
	   * writable as dynamically certificates of partners are added.
	   *
	   * @param aKeyStoreFile
	   *        The existing key store file. Must exist and may not be
	   *        <code>null</code>.
	   * @param sKeyStorePassword
	   *        The password to the key store. May not be <code>null</code> but
	   *        empty.
	   * @return this for chaining
	   */
	  
	// TODO  Need to define behavior for defualth keystore and multiiple keystores, right now defualth keystore is used in constructor
	  
	  @Nonnull
	  public SendAS2Message setPKCS12KeyStore (@Nullable final File aKeyStoreFile,
	                                             @Nullable final String sKeyStorePassword)
	  {
	    m_aKeyStoreFile = aKeyStoreFile;
	    m_sKeyStorePassword = sKeyStorePassword;
	    return this;
	  }

	  /**
	   * Change the behavior if all changes to the keystore should trigger a saving
	   * to the original file.
	   *
	   * @param bSaveKeyStoreChangesToFile
	   *        <code>true</code> if key store changes should be written back to the
	   *        file, <code>false</code> if not.
	   * @return this for chaining
	   */
	  @Nonnull
	  public SendAS2Message setSaveKeyStoreChangesToFile (final boolean bSaveKeyStoreChangesToFile)
	  {
	    m_bSaveKeyStoreChangesToFile = bSaveKeyStoreChangesToFile;
	    return this;
	  }

	  /**
	   * Set the subject for the AS2 message. By default
	   * {@value #DEFAULT_AS2_SUBJECT} is used so you don't need to set it.
	   *
	   * @param sAS2Subject
	   *        The new AS2 subject. May not be <code>null</code>.
	   * @return this for chaining
	   */
	  @Nonnull
	  public SendAS2Message setAS2Subject (@Nullable final String sAS2Subject)
	  {
	    m_sAS2Subject = sAS2Subject;
	    return this;
	  }

	  /**
	   * Set the AS2 sender ID (your ID). It is mapped to the "AS2-From" header. For
	   * PEPPOL the AS2 sender ID must be the common name (CN) of the sender's AP
	   * certificate subject. Therefore it usually starts with "APP_".
	   *
	   * @param sSenderAS2ID
	   *        The AS2 sender ID to be used. May not be <code>null</code>.
	   * @return this for chaining
	   */
	  @Nonnull
	  public SendAS2Message setSenderAS2ID (@Nullable final String sSenderAS2ID)
	  {
	    m_sSenderAS2ID = sSenderAS2ID;
	    return this;
	  }

	  /**
	   * Set the email address of the sender. This is required for the AS2 protocol
	   * but not (to my knowledge) used in PEPPOL.
	   *
	   * @param sSenderAS2Email
	   *        The email address of the sender. May not be <code>null</code> and
	   *        must be a valid email address.
	   * @return this for chaining
	   */
	  @Nonnull
	  public SendAS2Message setSenderAS2Email (@Nullable final String sSenderAS2Email)
	  {
	    m_sSenderAS2Email = sSenderAS2Email;
	    return this;
	  }

	  /**
	   * Set the key alias of the sender's key in the key store (see
	   * {@link #setPKCS12KeyStore(File, String)}). For PEPPOL the key alias of the
	   * sender should be identical to the AS2 sender ID (
	   * {@link #setSenderAS2ID(String)}), so it should also start with "APP_" (I
	   * think case insensitive for PKCS12 key stores).
	   *
	   * @param sSenderAS2KeyAlias
	   *        The sender key alias to be used. May not be <code>null</code>.
	   * @return this for chaining
	   */
	  @Nonnull
	  public SendAS2Message setSenderAS2KeyAlias (@Nullable final String sSenderAS2KeyAlias)
	  {
	    m_sSenderAS2KeyAlias = sSenderAS2KeyAlias;
	    return this;
	  }

	  /**
	   * Set the AS2 receiver ID (recipient ID). It is mapped to the "AS2-To"
	   * header. For PEPPOL the AS2 receiver ID must be the common name (CN) of the
	   * receiver's AP certificate subject (as determined by the SMP query).
	   * Therefore it usually starts with "APP_".
	   *
	   * @param sReceiverAS2ID
	   *        The AS2 receiver ID to be used. May not be <code>null</code>.
	   * @return this for chaining
	   */
	  @Nonnull
	  public SendAS2Message setReceiverAS2ID (@Nullable final String sReceiverAS2ID)
	  {
	    m_sReceiverAS2ID = sReceiverAS2ID;
	    return this;
	  }

	  /**
	   * Set the key alias of the receiver's key in the key store (see
	   * {@link #setPKCS12KeyStore(File, String)}). For PEPPOL the key alias of the
	   * receiver should be identical to the AS2 receiver ID (
	   * {@link #setReceiverAS2ID(String)}), so it should also start with "APP_" (I
	   * think case insensitive for PKCS12 key stores).
	   *
	   * @param sReceiverAS2KeyAlias
	   *        The receiver key alias to be used. May not be <code>null</code>.
	   * @return this for chaining
	   */
	  @Nonnull
	  public SendAS2Message setReceiverAS2KeyAlias (@Nullable final String sReceiverAS2KeyAlias)
	  {
	    m_sReceiverAS2KeyAlias = sReceiverAS2KeyAlias;
	    return this;
	  }

	  /**
	   * Set the AS2 endpoint URL of the receiver. This URL should be determined by
	   * an SMP query.
	   *
	   * @param sReceiverAS2Url
	   *        The AS2 endpoint URL of the receiver. This must be a valid URL. May
	   *        not be <code>null</code>.
	   * @return this for chaining
	   */
	  @Nonnull
	  public SendAS2Message setReceiverAS2Url (@Nullable final String sReceiverAS2Url)
	  {
	    m_sReceiverAS2Url = sReceiverAS2Url;
	    return this;
	  }

	  /**
	   * Set the public certificate of the receiver as determined by the SMP query.
	   *
	   * @param aReceiverCert
	   *        The receiver certificate. May not be <code>null</code>.
	   * @return this for chaining
	   */
	  @Nonnull
	  public SendAS2Message setReceiverCertificate (@Nullable final X509Certificate aReceiverCert)
	  {
	    m_aReceiverCert = aReceiverCert;
	    return this;
	  }

	  /**
	 * @return the m_aPayload
	 */
	public File getPayload() {
		return m_aPayload;
	}


	/**
	 * @param m_aPayload the m_aPayload to set
	 */
	public void setPayload(File m_aPayload) {
		this.m_aPayload = m_aPayload;
	}


	/**
	   * Set the algorithm to be used to sign AS2 messages. By default
	   * {@link #DEFAULT_SIGNING_ALGORITHM} is used. An encryption algorithm cannot
	   * be set because according to the PEPPOL AS2 specification the AS2 messages
	   * may not be encrypted on a business level.
	   *
	   * @param eSigningAlgo
	   *        The signing algorithm to be used. May not be <code>null</code>.
	   * @return this for chaining
	   */
	  @Nonnull
	  public SendAS2Message setAS2SigningAlgorithm (@Nullable final ECryptoAlgorithmSign eSigningAlgo)
	  {
	    m_eSigningAlgo = eSigningAlgo;
	    return this;
	  }

	  /**
	   * Set the abstract format for AS2 message IDs. By default
	   * {@link #DEFAULT_AS2_MESSAGE_ID_FORMAT} is used so there is no need to
	   * change it. The replacement of placeholders depends on the underlying AS2
	   * library.
	   *
	   * @param sMessageIDFormat
	   *        The message ID format to be used. May not be <code>null</code>.
	   * @return this for chaining
	   */
	  @Nonnull
	  public SendAS2Message setAS2MessageIDFormat (@Nullable final String sMessageIDFormat)
	  {
	    m_sMessageIDFormat = sMessageIDFormat;
	    return this;
	  }
	  
	  @Nonnull
	  public SendAS2Message setAS2ClientFactory (@Nonnull final IFactory <AS2Client> aAS2ClientFactory)
	  {
	    m_aAS2ClientFactory = ValueEnforcer.notNull (aAS2ClientFactory, "AS2ClientFactory");
	    return this;
	  }
	  
	  /**
	   * Certain values can by convention be derived from other values. This happens
	   * inside this method. There is no need to call this method manually, it is
	   * called automatically before {@link #verifyContent()} is called.
	   */
	  @OverridingMethodsMustInvokeSuper
	  protected void setDefaultDerivedValues ()
	  {
	    if (m_sReceiverAS2KeyAlias == null)
	    {
	      // No key alias is specified, so use the same as the receiver ID (which
	      // may be null)
	      m_sReceiverAS2KeyAlias = m_sReceiverAS2ID;
	      if (s_aLogger.isDebugEnabled ())
	        s_aLogger.debug ("The receiver AS2 key alias was defaulted to the AS2 receiver ID");
	    }
	  }
	  
	  /**
	   * Verify the content of all contained fields so that all know issues are
	   * captured before sending. This method is automatically called before the
	   * message is send (see {@link #sendSynchronous()}). All verification warnings
	   * and errors are handled via the message handler.
	   *
	   * @throws AS2ClientBuilderException
	   *         In case the message handler throws an exception in case of an
	   *         error.
	   * @see #setMessageHandler(IAS2ClientBuilderMessageHandler)
	   */
	  public void verifyContent () throws AS2ClientBuilderException
	  {
	    if (m_aKeyStoreFile == null)
	      m_aMessageHandler.error ("No AS2 key store is defined");
	    else
	    {
	      if (!m_aKeyStoreFile.exists ())
	        m_aMessageHandler.error ("The provided AS2 key store '" +
	                                 m_aKeyStoreFile.getAbsolutePath () +
	                                 "' does not exist.");
	      else
	        if (!m_aKeyStoreFile.isFile ())
	          m_aMessageHandler.error ("The provided AS2 key store '" +
	                                   m_aKeyStoreFile.getAbsolutePath () +
	                                   "' is not a file but potentially a directory.");
	        else
	          if (!m_aKeyStoreFile.canWrite ())
	            m_aMessageHandler.error ("The provided AS2 key store '" +
	                                     m_aKeyStoreFile.getAbsolutePath () +
	                                     "' is not writable. As it is dynamically modified, it must be writable.");

	    }
	    if (m_sKeyStorePassword == null)
	      m_aMessageHandler.error ("No key store password provided. If you need an empty password, please provide an empty String!");

	    if (StringHelper.hasNoText (m_sAS2Subject))
	      m_aMessageHandler.error ("The AS2 message subject is missing");

	    if (StringHelper.hasNoText (m_sSenderAS2ID))
	      m_aMessageHandler.error ("The AS2 sender ID is missing");
	    else
	      if (!m_sSenderAS2ID.startsWith ("APP_"))
	        m_aMessageHandler.warn ("The AS2 sender ID '" +
	                                m_sSenderAS2ID +
	                                "' should start with 'APP_' as required by the PEPPOL specification");

	    if (StringHelper.hasNoText (m_sSenderAS2Email))
	      m_aMessageHandler.error ("The AS2 sender email address is missing");
	    else
	      if (!EmailAddressHelper.isValid (m_sSenderAS2Email))
	        m_aMessageHandler.warn ("The AS2 sender email address '" +
	                                m_sSenderAS2Email +
	                                "' seems to be an invalid email address.");

	    if (StringHelper.hasNoText (m_sSenderAS2KeyAlias))
	      m_aMessageHandler.error ("The AS2 sender key alias is missing");
	    else
	      if (!m_sSenderAS2KeyAlias.startsWith ("APP_"))
	        m_aMessageHandler.warn ("The AS2 sender key alias '" +
	                                m_sSenderAS2KeyAlias +
	                                "' should start with 'APP_' for the use with the dynamic AS2 partnerships");
	      else
	        if (m_sSenderAS2ID != null && !m_sSenderAS2ID.equals (m_sSenderAS2KeyAlias))
	          m_aMessageHandler.warn ("The AS2 sender key alias ('" +
	                                  m_sSenderAS2KeyAlias +
	                                  "') should match the AS2 sender ID ('" +
	                                  m_sSenderAS2ID +
	                                  "')");

	    if (StringHelper.hasNoText (m_sReceiverAS2ID))
	      m_aMessageHandler.error ("The AS2 receiver ID is missing");
	    else
	      if (!m_sReceiverAS2ID.startsWith ("APP_"))
	        m_aMessageHandler.warn ("The AS2 receiver ID '" +
	                                m_sReceiverAS2ID +
	                                "' should start with 'APP_' as required by the PEPPOL specification");

	    if (StringHelper.hasNoText (m_sReceiverAS2KeyAlias))
	      m_aMessageHandler.error ("The AS2 receiver key alias is missing");
	    else
	      if (!m_sReceiverAS2KeyAlias.startsWith ("APP_"))
	        m_aMessageHandler.warn ("The AS2 receiver key alias '" +
	                                m_sReceiverAS2KeyAlias +
	                                "' should start with 'APP_' for the use with the dynamic AS2 partnerships");
	      else
	        if (m_sReceiverAS2ID != null && !m_sReceiverAS2ID.equals (m_sReceiverAS2KeyAlias))
	          m_aMessageHandler.warn ("The AS2 receiver key alias ('" +
	                                  m_sReceiverAS2KeyAlias +
	                                  "') should match the AS2 receiver ID ('" +
	                                  m_sReceiverAS2ID +
	                                  "')");

	    if (StringHelper.hasNoText (m_sReceiverAS2Url))
	      m_aMessageHandler.error ("The AS2 receiver URL (AS2 endpoint URL) is missing");
	    else
	      if (URLHelper.getAsURL (m_sReceiverAS2Url) == null)
	        m_aMessageHandler.warn ("The provided AS2 receiver URL '" + m_sReceiverAS2Url + "' seems to be an invalid URL");

	    if (m_aReceiverCert == null)
	      m_aMessageHandler.error ("The receiver X.509 certificate is missing. Usually this is extracted from the SMP response");

	    if (m_eSigningAlgo == null)
	      m_aMessageHandler.error ("The signing algorithm for the AS2 message is missing");

	    if (StringHelper.hasNoText (m_sMessageIDFormat))
	      m_aMessageHandler.error ("The AS2 message ID format is missing.");
   
	  }
	  
	  /**
	   * This is the main sending routine. It performs the following steps:
	   * <ol>
	   * <li>Verify that all required parameters are present and valid -
	   * {@link #verifyContent()}</li>
	   * <li>The business document is read as XML. In case of an error, an exception
	   * is thrown.</li>
	   * <li>The Standard Business Document (SBD) is created, all PEPPOL required
	   * fields are set and the business document is embedded.</li>
	   * <li>The SBD is serialized and send via AS2</li>
	   * <li>The AS2 response incl. the MDN is returned for further evaluation.</li>
	   * </ol>
	   *
	   * @return The AS2 response returned by the AS2 sender. This is never
	   *         <code>null</code>.
	   * @throws AS2ClientBuilderException
	   *         In case the the business document is invalid XML or in case
	   *         {@link #verifyContent()} throws an exception because of invalid or
	   *         incomplete settings.
	   */
	  @Nonnull
	  public AS2ClientResponse sendSynchronous () throws AS2ClientBuilderException
	  {
		  
//		  s_aLogger.error(m_aKeyStoreFile.toString());
		  
		
	    // Set derivable values
	    setDefaultDerivedValues ();

	    // Verify the whole data set
	    verifyContent ();

	    // Build message

	    // 1 / 2. read business document
	  

	    // 4. send message
	    // Start building the AS2 client settings
	    final AS2ClientSettings aAS2ClientSettings = new AS2ClientSettings ();
	    // Key store
	    
	    if (m_aKeyStoreFile.isFile())
	    {
	    	s_aLogger.error("Keystore is file and the filename of this = " + m_aKeyStoreFile.getName() + "and the path = " + m_aKeyStoreFile.getAbsolutePath());
	    } else { 
	    	s_aLogger.error("Keystore is not a file = " + m_aKeyStoreFile.getName() + "and the path = " + m_aKeyStoreFile.getAbsolutePath());
	    }
	    aAS2ClientSettings.setKeyStore (m_aKeyStoreFile, m_sKeyStorePassword);
	    aAS2ClientSettings.setSaveKeyStoreChangesToFile (m_bSaveKeyStoreChangesToFile);

	    // Fixed sender
	    aAS2ClientSettings.setSenderData (m_sSenderAS2ID, m_sSenderAS2Email, m_sSenderAS2KeyAlias);

	    // Dynamic receiver
	    aAS2ClientSettings.setReceiverData (m_sReceiverAS2ID, m_sReceiverAS2KeyAlias, m_sReceiverAS2Url);
	    aAS2ClientSettings.setReceiverCertificate (m_aReceiverCert);

	    // AS2 stuff - no need to change anything in this block
	    aAS2ClientSettings.setPartnershipName (aAS2ClientSettings.getSenderAS2ID () +
	                                           "-" +
	                                           aAS2ClientSettings.getReceiverAS2ID ());
	    aAS2ClientSettings.setMDNOptions (new DispositionOptions ().setMICAlg (m_eSigningAlgo)
	                                                               .setMICAlgImportance (DispositionOptions.IMPORTANCE_REQUIRED)
	                                                               .setProtocol (DispositionOptions.PROTOCOL_PKCS7_SIGNATURE)
	                                                               .setProtocolImportance (DispositionOptions.IMPORTANCE_REQUIRED));
	    aAS2ClientSettings.setEncryptAndSign (null, m_eSigningAlgo);
	    aAS2ClientSettings.setMessageIDFormat (m_sMessageIDFormat);

	    final AS2ClientRequest aRequest = new AS2ClientRequest (m_sAS2Subject);
	    // Using a String is better when having a
	    // com.sun.xml.ws.encoding.XmlDataContentHandler installed!
	    
	    //arequest.set data is used for palod
	    
//	    aRequest.setData (aBAOS.getAsString (CCharset.CHARSET_UTF_8_OBJ), CCharset.CHARSET_UTF_8_OBJ);

	    aRequest.setData (m_aPayload);
		   s_aLogger.error("payload on sendas2message class is : " + m_aPayload.toString());
	    
	    
	    final AS2Client aAS2Client = m_aAS2ClientFactory.create ();
	    final AS2ClientResponse aResponse = aAS2Client.sendSynchronous (aAS2ClientSettings, aRequest);
	    return aResponse;
	  }

}
